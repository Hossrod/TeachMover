using System;
using System.IO.Ports;
using System.Windows.Forms;

namespace TeachMover_Sample_App
{
    public partial class Form1 : Form
    {
        SerialPort ComPort;
        string TMSpeed      = "232";
        string TMBase       = "0";
        string TMShoulder   = "0";
        string TMElbow      = "0";
        string TMRightWrist = "0";
        string TMLeftWrist  = "0";
        string TMGripper    = "0";
        string TMOut        = "0";
        string ReadBuffer   = "";

        /// <summary>
        /// When the serial port receives text, it fires an event which calls this function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnSerialDataReceived(object sender, SerialDataReceivedEventArgs args)
        {
            // Because serial port stuff is run in non-GUI thread, the following line
            // is needed to call into a function that is running in GUI thread.
            this.Invoke(new EventHandler(ReadComPort));
        }

        /// <summary>
        /// This function reads data from the comport. Its designed to be called by the
        /// OnSerialDataReceived function. It should not be called directly from elsewhere.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ReadComPort(object s, EventArgs e)
        {
            // Grab text currently in comport receive buffer
            string data = ComPort.ReadExisting();
            
            // Because the incoming data can be spread out across multiple receive events,
            // a buffer must be used to store each part until a full string is completed.
            // The end of the string is determined by checking if the last character of the
            // string is '\r' as this is the how the robot signifies the end of a string. 
            ReadBuffer += data;

            if (ReadBuffer.EndsWith("\r"))
            {
                // A full string has been completed, so print to log and clear buffer
                Log(ReadBuffer, "Robot");
                ReadBuffer = "";
            }
        }

        /// <summary>
        /// Visual Studio generated code
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitializeComPort("COM1", 9600);
            UpdateJointSteps();
        }

        /// <summary>
        /// Initializes the comport object. Only the port and baud are settable.
        /// </summary>
        /// <param name="port"></param>
        /// <param name="baud"></param>
        private void InitializeComPort(string port, int baud)
        {
            ComPort = new SerialPort(port, baud);
            ComPort.Parity = Parity.None;
            ComPort.StopBits = StopBits.One;
            ComPort.DataBits = 8;
            ComPort.Handshake = Handshake.None;
            ComPort.DataReceived += OnSerialDataReceived;
            ComPort.Open();
        }

        /// <summary>
        /// When the Submit button is pressed, the text in textBoxRobotCommand is sent
        /// to the robot.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSubmitCommand_Click(object sender, EventArgs e)
        {
            Log(textBoxRobotCommand.Text.ToString(), "PC");
            RobotCommand(textBoxRobotCommand.Text.ToString());
        }

        /// <summary>
        /// This function simply writes out a command its passed, out to the robot.
        /// </summary>
        /// <param name="text"></param>
        private void RobotCommand(string text)
        {
            ComPort.Write(text + '\r');
        }

        /// <summary>
        /// This function updates the textBoxRobotCommand text with a @STEP command using global
        /// step counters. This function is typically called immediatly after updating any of the
        /// global step variables.
        /// </summary>
        private void UpdateStepString()
        {
            textBoxRobotCommand.Text = "@STEP " + TMSpeed + "," + TMBase + "," + TMShoulder + "," + TMElbow + "," + TMRightWrist + "," + TMLeftWrist + "," + TMGripper + "," + TMOut;
        }

        /// <summary>
        /// Simply resets all global joint step counters to 0.
        /// </summary>
        private void ResetJoints()
        {
            TMBase       = "0";
            TMShoulder   = "0";
            TMElbow      = "0";
            TMRightWrist = "0";
            TMLeftWrist  = "0";
            TMGripper    = "0";
        }

        private void UpdateJointSteps()
        {
            textBoxBaseSteps.Text       = TMBase;
            textBoxShoulderSteps.Text   = TMShoulder;
            textBoxElbowSteps.Text      = TMElbow;
            textBoxRightWristSteps.Text = TMRightWrist;
            textBoxLeftWristSteps.Text  = TMLeftWrist;
            textBoxGripperSteps.Text    = TMGripper;
        }

        private void Log(string text, string source)
        {
            string timestamp = DateTime.Now.ToString();
            this.textBoxLog.Text += timestamp + " :: " + source + " >> " + text + Environment.NewLine;
            this.textBoxLog.SelectionStart = this.textBoxLog.TextLength - 1;
            this.textBoxLog.ScrollToCaret();
            listBoxLog.Items.Add(timestamp + " :: " + source + " >> " + text + Environment.NewLine);
            listBoxLog.SelectedValue = listBoxLog.Items.Count - 1;
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            Log("@RESET", "PC"); 
            RobotCommand("@RESET");
        }

        private void buttonRead_Click(object sender, EventArgs e)
        {
            Log("@READ", "PC");
            RobotCommand("@READ");
        }

        private void buttonCloseGripper_Click_1(object sender, EventArgs e)
        {
            Log("@CLOSE", "PC");
            RobotCommand("@CLOSE");
        }

        private void textBoxBaseSteps_TextChanged(object sender, EventArgs e)
        {
            if (textBoxBaseSteps.Text == "")
                textBoxBaseSteps.Text = "0";
            TMBase = textBoxBaseSteps.Text;
            UpdateStepString();
        }

        private void textBoxShoulderSteps_TextChanged(object sender, EventArgs e)
        {
            TMShoulder = textBoxShoulderSteps.Text;
            UpdateStepString();
        }

        private void textBoxElbowSteps_TextChanged(object sender, EventArgs e)
        {
            TMElbow = textBoxElbowSteps.Text;
            UpdateStepString();
        }

        private void textBoxRightWristSteps_TextChanged(object sender, EventArgs e)
        {
            TMRightWrist = textBoxRightWristSteps.Text;
            UpdateStepString();
        }

        private void textBoxLeftWristSteps_TextChanged(object sender, EventArgs e)
        {
            TMLeftWrist = textBoxLeftWristSteps.Text;
            UpdateStepString();
        }

        private void textBoxGripperSteps_TextChanged(object sender, EventArgs e)
        {
            TMGripper = textBoxGripperSteps.Text;
            UpdateStepString();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ResetJoints();
            UpdateJointSteps();
        }

    }
}