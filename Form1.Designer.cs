namespace TeachMover_Sample_App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.buttonSubmitCommand = new System.Windows.Forms.Button();
            this.textBoxRobotCommand = new System.Windows.Forms.TextBox();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonOpenPort = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxBaud = new System.Windows.Forms.ComboBox();
            this.comboBoxPort = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonRead = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.checkBoxGripperDirection = new System.Windows.Forms.CheckBox();
            this.textBoxGripperSteps = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.checkBoxLeftWristDirection = new System.Windows.Forms.CheckBox();
            this.textBoxLeftWristSteps = new System.Windows.Forms.TextBox();
            this.textBoxRightWristSteps = new System.Windows.Forms.TextBox();
            this.checkBoxRightWristDirection = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxElbowSteps = new System.Windows.Forms.TextBox();
            this.checkBoxElbowDirection = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxShoulderSteps = new System.Windows.Forms.TextBox();
            this.checkBoxShoulderDirection = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBaseSteps = new System.Windows.Forms.TextBox();
            this.checkBoxBaseDirection = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonCloseGripper = new System.Windows.Forms.Button();
            this.checkBoxRequireSubmit = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelSpeed = new System.Windows.Forms.Label();
            this.comboBoxSpeed = new System.Windows.Forms.ComboBox();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSubmitCommand
            // 
            this.buttonSubmitCommand.Location = new System.Drawing.Point(6, 182);
            this.buttonSubmitCommand.Name = "buttonSubmitCommand";
            this.buttonSubmitCommand.Size = new System.Drawing.Size(148, 23);
            this.buttonSubmitCommand.TabIndex = 0;
            this.buttonSubmitCommand.Text = "Submit @STEP Command";
            this.buttonSubmitCommand.UseVisualStyleBackColor = true;
            this.buttonSubmitCommand.Click += new System.EventHandler(this.buttonSubmitCommand_Click);
            // 
            // textBoxRobotCommand
            // 
            this.textBoxRobotCommand.Location = new System.Drawing.Point(6, 156);
            this.textBoxRobotCommand.Name = "textBoxRobotCommand";
            this.textBoxRobotCommand.Size = new System.Drawing.Size(222, 20);
            this.textBoxRobotCommand.TabIndex = 1;
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(6, 19);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(364, 79);
            this.textBoxLog.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxLog);
            this.groupBox1.Location = new System.Drawing.Point(12, 290);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(379, 112);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Communications Log";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonOpenPort);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.comboBoxBaud);
            this.groupBox2.Controls.Add(this.comboBoxPort);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(136, 105);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Config";
            // 
            // buttonOpenPort
            // 
            this.buttonOpenPort.Location = new System.Drawing.Point(9, 73);
            this.buttonOpenPort.Name = "buttonOpenPort";
            this.buttonOpenPort.Size = new System.Drawing.Size(119, 23);
            this.buttonOpenPort.TabIndex = 6;
            this.buttonOpenPort.Text = "Open Port";
            this.buttonOpenPort.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Baud";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Port";
            // 
            // comboBoxBaud
            // 
            this.comboBoxBaud.FormattingEnabled = true;
            this.comboBoxBaud.Items.AddRange(new object[] {
            "1200",
            "2400",
            "4800",
            "9600"});
            this.comboBoxBaud.Location = new System.Drawing.Point(44, 46);
            this.comboBoxBaud.Name = "comboBoxBaud";
            this.comboBoxBaud.Size = new System.Drawing.Size(84, 21);
            this.comboBoxBaud.TabIndex = 8;
            // 
            // comboBoxPort
            // 
            this.comboBoxPort.FormattingEnabled = true;
            this.comboBoxPort.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4"});
            this.comboBoxPort.Location = new System.Drawing.Point(44, 19);
            this.comboBoxPort.Name = "comboBoxPort";
            this.comboBoxPort.Size = new System.Drawing.Size(84, 21);
            this.comboBoxPort.TabIndex = 7;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonClear);
            this.groupBox3.Controls.Add(this.buttonRead);
            this.groupBox3.Controls.Add(this.buttonReset);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.checkBoxGripperDirection);
            this.groupBox3.Controls.Add(this.textBoxGripperSteps);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.checkBoxLeftWristDirection);
            this.groupBox3.Controls.Add(this.textBoxLeftWristSteps);
            this.groupBox3.Controls.Add(this.textBoxRightWristSteps);
            this.groupBox3.Controls.Add(this.checkBoxRightWristDirection);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.textBoxElbowSteps);
            this.groupBox3.Controls.Add(this.checkBoxElbowDirection);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.textBoxShoulderSteps);
            this.groupBox3.Controls.Add(this.checkBoxShoulderDirection);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.textBoxBaseSteps);
            this.groupBox3.Controls.Add(this.checkBoxBaseDirection);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.buttonCloseGripper);
            this.groupBox3.Controls.Add(this.textBoxRobotCommand);
            this.groupBox3.Controls.Add(this.buttonSubmitCommand);
            this.groupBox3.Location = new System.Drawing.Point(154, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(237, 256);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Robot Commands";
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(157, 182);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(69, 23);
            this.buttonClear.TabIndex = 28;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonRead
            // 
            this.buttonRead.Location = new System.Drawing.Point(157, 211);
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.Size = new System.Drawing.Size(69, 23);
            this.buttonRead.TabIndex = 27;
            this.buttonRead.Text = "Read";
            this.buttonRead.UseVisualStyleBackColor = true;
            this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(100, 211);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(54, 23);
            this.buttonReset.TabIndex = 26;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 136);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Gripper";
            // 
            // checkBoxGripperDirection
            // 
            this.checkBoxGripperDirection.AutoSize = true;
            this.checkBoxGripperDirection.Location = new System.Drawing.Point(71, 135);
            this.checkBoxGripperDirection.Name = "checkBoxGripperDirection";
            this.checkBoxGripperDirection.Size = new System.Drawing.Size(83, 17);
            this.checkBoxGripperDirection.TabIndex = 24;
            this.checkBoxGripperDirection.Text = "Open/Close";
            this.checkBoxGripperDirection.UseVisualStyleBackColor = true;
            // 
            // textBoxGripperSteps
            // 
            this.textBoxGripperSteps.Location = new System.Drawing.Point(157, 133);
            this.textBoxGripperSteps.Name = "textBoxGripperSteps";
            this.textBoxGripperSteps.Size = new System.Drawing.Size(71, 20);
            this.textBoxGripperSteps.TabIndex = 23;
            this.textBoxGripperSteps.TextChanged += new System.EventHandler(this.textBoxGripperSteps_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 117);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Left Wrist";
            // 
            // checkBoxLeftWristDirection
            // 
            this.checkBoxLeftWristDirection.AutoSize = true;
            this.checkBoxLeftWristDirection.Location = new System.Drawing.Point(71, 116);
            this.checkBoxLeftWristDirection.Name = "checkBoxLeftWristDirection";
            this.checkBoxLeftWristDirection.Size = new System.Drawing.Size(73, 17);
            this.checkBoxLeftWristDirection.TabIndex = 21;
            this.checkBoxLeftWristDirection.Text = "Down/Up";
            this.checkBoxLeftWristDirection.UseVisualStyleBackColor = true;
            // 
            // textBoxLeftWristSteps
            // 
            this.textBoxLeftWristSteps.Location = new System.Drawing.Point(157, 114);
            this.textBoxLeftWristSteps.Name = "textBoxLeftWristSteps";
            this.textBoxLeftWristSteps.Size = new System.Drawing.Size(71, 20);
            this.textBoxLeftWristSteps.TabIndex = 20;
            this.textBoxLeftWristSteps.TextChanged += new System.EventHandler(this.textBoxLeftWristSteps_TextChanged);
            // 
            // textBoxRightWristSteps
            // 
            this.textBoxRightWristSteps.Location = new System.Drawing.Point(157, 95);
            this.textBoxRightWristSteps.Name = "textBoxRightWristSteps";
            this.textBoxRightWristSteps.Size = new System.Drawing.Size(71, 20);
            this.textBoxRightWristSteps.TabIndex = 19;
            this.textBoxRightWristSteps.TextChanged += new System.EventHandler(this.textBoxRightWristSteps_TextChanged);
            // 
            // checkBoxRightWristDirection
            // 
            this.checkBoxRightWristDirection.AutoSize = true;
            this.checkBoxRightWristDirection.Location = new System.Drawing.Point(71, 97);
            this.checkBoxRightWristDirection.Name = "checkBoxRightWristDirection";
            this.checkBoxRightWristDirection.Size = new System.Drawing.Size(73, 17);
            this.checkBoxRightWristDirection.TabIndex = 18;
            this.checkBoxRightWristDirection.Text = "Down/Up";
            this.checkBoxRightWristDirection.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Right Wrist";
            // 
            // textBoxElbowSteps
            // 
            this.textBoxElbowSteps.Location = new System.Drawing.Point(157, 76);
            this.textBoxElbowSteps.Name = "textBoxElbowSteps";
            this.textBoxElbowSteps.Size = new System.Drawing.Size(71, 20);
            this.textBoxElbowSteps.TabIndex = 16;
            this.textBoxElbowSteps.TextChanged += new System.EventHandler(this.textBoxElbowSteps_TextChanged);
            // 
            // checkBoxElbowDirection
            // 
            this.checkBoxElbowDirection.AutoSize = true;
            this.checkBoxElbowDirection.Location = new System.Drawing.Point(71, 78);
            this.checkBoxElbowDirection.Name = "checkBoxElbowDirection";
            this.checkBoxElbowDirection.Size = new System.Drawing.Size(73, 17);
            this.checkBoxElbowDirection.TabIndex = 15;
            this.checkBoxElbowDirection.Text = "Down/Up";
            this.checkBoxElbowDirection.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Elbow";
            // 
            // textBoxShoulderSteps
            // 
            this.textBoxShoulderSteps.Location = new System.Drawing.Point(157, 56);
            this.textBoxShoulderSteps.Name = "textBoxShoulderSteps";
            this.textBoxShoulderSteps.Size = new System.Drawing.Size(71, 20);
            this.textBoxShoulderSteps.TabIndex = 13;
            this.textBoxShoulderSteps.TextChanged += new System.EventHandler(this.textBoxShoulderSteps_TextChanged);
            // 
            // checkBoxShoulderDirection
            // 
            this.checkBoxShoulderDirection.AutoSize = true;
            this.checkBoxShoulderDirection.Location = new System.Drawing.Point(71, 58);
            this.checkBoxShoulderDirection.Name = "checkBoxShoulderDirection";
            this.checkBoxShoulderDirection.Size = new System.Drawing.Size(73, 17);
            this.checkBoxShoulderDirection.TabIndex = 12;
            this.checkBoxShoulderDirection.Text = "Down/Up";
            this.checkBoxShoulderDirection.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Shoulder";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(154, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Half-Steps";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(68, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Direction";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Joint";
            // 
            // textBoxBaseSteps
            // 
            this.textBoxBaseSteps.Location = new System.Drawing.Point(157, 36);
            this.textBoxBaseSteps.MaxLength = 5;
            this.textBoxBaseSteps.Name = "textBoxBaseSteps";
            this.textBoxBaseSteps.Size = new System.Drawing.Size(71, 20);
            this.textBoxBaseSteps.TabIndex = 7;
            this.textBoxBaseSteps.TextChanged += new System.EventHandler(this.textBoxBaseSteps_TextChanged);
            // 
            // checkBoxBaseDirection
            // 
            this.checkBoxBaseDirection.AutoSize = true;
            this.checkBoxBaseDirection.Location = new System.Drawing.Point(71, 38);
            this.checkBoxBaseDirection.Name = "checkBoxBaseDirection";
            this.checkBoxBaseDirection.Size = new System.Drawing.Size(74, 17);
            this.checkBoxBaseDirection.TabIndex = 6;
            this.checkBoxBaseDirection.Text = "CCW/CW";
            this.checkBoxBaseDirection.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Base";
            // 
            // buttonCloseGripper
            // 
            this.buttonCloseGripper.Location = new System.Drawing.Point(6, 211);
            this.buttonCloseGripper.Name = "buttonCloseGripper";
            this.buttonCloseGripper.Size = new System.Drawing.Size(88, 23);
            this.buttonCloseGripper.TabIndex = 2;
            this.buttonCloseGripper.Text = "Close Gripper";
            this.buttonCloseGripper.UseVisualStyleBackColor = true;
            this.buttonCloseGripper.Click += new System.EventHandler(this.buttonCloseGripper_Click_1);
            // 
            // checkBoxRequireSubmit
            // 
            this.checkBoxRequireSubmit.AutoSize = true;
            this.checkBoxRequireSubmit.Location = new System.Drawing.Point(12, 19);
            this.checkBoxRequireSubmit.Name = "checkBoxRequireSubmit";
            this.checkBoxRequireSubmit.Size = new System.Drawing.Size(98, 17);
            this.checkBoxRequireSubmit.TabIndex = 11;
            this.checkBoxRequireSubmit.Text = "Require Submit";
            this.checkBoxRequireSubmit.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelSpeed);
            this.groupBox4.Controls.Add(this.comboBoxSpeed);
            this.groupBox4.Controls.Add(this.checkBoxRequireSubmit);
            this.groupBox4.Location = new System.Drawing.Point(12, 123);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(136, 100);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Options";
            // 
            // labelSpeed
            // 
            this.labelSpeed.AutoSize = true;
            this.labelSpeed.Location = new System.Drawing.Point(9, 43);
            this.labelSpeed.Name = "labelSpeed";
            this.labelSpeed.Size = new System.Drawing.Size(38, 13);
            this.labelSpeed.TabIndex = 13;
            this.labelSpeed.Text = "Speed";
            // 
            // comboBoxSpeed
            // 
            this.comboBoxSpeed.FormattingEnabled = true;
            this.comboBoxSpeed.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
            this.comboBoxSpeed.Location = new System.Drawing.Point(53, 40);
            this.comboBoxSpeed.Name = "comboBoxSpeed";
            this.comboBoxSpeed.Size = new System.Drawing.Size(75, 21);
            this.comboBoxSpeed.TabIndex = 12;
            // 
            // listBoxLog
            // 
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(18, 414);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.ScrollAlwaysVisible = true;
            this.listBoxLog.Size = new System.Drawing.Size(364, 95);
            this.listBoxLog.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 521);
            this.Controls.Add(this.listBoxLog);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "TeachMover Sample App :: By Hoss";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonSubmitCommand;
        private System.Windows.Forms.TextBox textBoxRobotCommand;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxBaud;
        private System.Windows.Forms.Button buttonOpenPort;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonCloseGripper;
        private System.Windows.Forms.CheckBox checkBoxRequireSubmit;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBoxSpeed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxBaseSteps;
        private System.Windows.Forms.CheckBox checkBoxBaseDirection;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxRightWristSteps;
        private System.Windows.Forms.CheckBox checkBoxRightWristDirection;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxElbowSteps;
        private System.Windows.Forms.CheckBox checkBoxElbowDirection;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxShoulderSteps;
        private System.Windows.Forms.CheckBox checkBoxShoulderDirection;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox checkBoxGripperDirection;
        private System.Windows.Forms.TextBox textBoxGripperSteps;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBoxLeftWristDirection;
        private System.Windows.Forms.TextBox textBoxLeftWristSteps;
        private System.Windows.Forms.Label labelSpeed;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonRead;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.Button buttonClear;

    }
}

