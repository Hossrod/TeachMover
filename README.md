# Synopsis
A simple Windows GUI app to control a Microbot TeachMover robot arm via its serial port.

![Microbot TeachMover](Assets/robot.png)

![UI](Assets/ui.png)

# Notes
* App is functional but not complete. For example, COM port selection in UI is not hooked up, hardcoded to COM1.
* App is very basic. For example, it is not intended to script/automate the arm. It's more of a proof of concept and to demonstrate how to talk to the arm via its serial protocol.
* App was orginally written in 2006, when I was learning C#. It is .NET 2.0 and I open and compiled it with VS 2017 before adding to gitlab. I last tested it on Windows 7 using an old USB to Serial device.

# About TeachMover
The [Microbot Teachmover](http://www.microbotzone.com/) was a fairly high end educational robot arm in the 80's and 90's. They still make and sell them today. The company Microbot was bought by [Questtech](http://questechzone.com/) in 1991 but the robot is still referred to as the Microbot.

They pop up on [craigslist](http://www.craigslist.org) and [ebay](http://www.ebay.com) quite often. I found mine on ebay in 2006 being sold by a college that no longer needed it. I think I paid about $200 for it. It worked great both via a serial port or the controller. Just needed a little oil in the joints.

[The Old Robots Website](http://www.theoldrobots.com/) maintains a [page about the TeachMover](http://www.theoldrobots.com/Teachmover.html) as well with some addtional information.

# Videos
[![TeachMover Video](https://img.youtube.com/vi/2WDhunxx0LE/0.jpg)](https://www.youtube.com/watch?v=2WDhunxx0LE)

Video I made of mine back in 2006.
 

[![TeachMover Video](https://img.youtube.com/vi/szDbJdPCEzQ/0.jpg)](https://www.youtube.com/watch?v=szDbJdPCEzQ)

TeachMover on TV from 1983.
 

[![TeachMover Video](https://img.youtube.com/vi/Spd17zqe8lE/0.jpg)](https://www.youtube.com/watch?v=Spd17zqe8lE)

TeachMover playing Towers Of Hanoi.
 

[![TeachMover Video](https://img.youtube.com/vi/bnhDGNO71PQ/0.jpg)](https://www.youtube.com/watch?v=bnhDGNO71PQ)

TeachMover enhanced block stacking program.
